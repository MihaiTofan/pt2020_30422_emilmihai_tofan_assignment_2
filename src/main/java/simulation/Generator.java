package simulation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

import objects.Client;

public class Generator {

	
	public static int rndRange(int start, int finish)
	{
	  Random rnd = new Random();
	  int randomNumber = start + rnd.nextInt(finish + 1 - start);
	  return randomNumber;

	}
	
	
	
	public static ArrayList<Integer> readFileInput(String s) throws IOException
    {
    	Path filePath = Paths.get(s);
    	
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(filePath);
		
		String string;
		ArrayList<Integer> integers = new ArrayList<Integer>();
		
		while(scanner.hasNext())
		{
			string  = scanner.nextLine();
			string = string.replaceAll("," , " ");
			String[] numbers = string.split(" ");
			for(int i=0; i< numbers.length;i++)
			{
				int number = Integer.parseInt(numbers[i]);
				integers.add(number);
			}
			
		}
		
		return integers;
    }
	
	
	
	public Client generateRandomClient(String inputFile) throws IOException
	{
		ArrayList<Integer> fileData = new ArrayList<Integer>();
		fileData = readFileInput(inputFile);
		
		Client c1 = new Client();
		
	
		c1.arrivalTime = rndRange( fileData.get(3), fileData.get(4));
		c1.serviceTime = rndRange( fileData.get(5), fileData.get(6));
		
		return c1;
	}
	
	
	
	public  ArrayList<Client> generateKClients(String inputFile) throws IOException
	{
		ArrayList<Integer> fileData = new ArrayList<Integer>();
		fileData = readFileInput(inputFile);
		
		ArrayList<Client> clients = new ArrayList<Client>();
		Client client = new Client();
		
		
		for(int i = 0; i < fileData.get(0); i++)
		{
			
			
			client = generateRandomClient(inputFile);
			client.ID = i;
			
			clients.add(i, client);
		}
		return clients;
	}
	
	public void printClientDetails(ArrayList<Client> clients)
	{
	
		
		if(clients.isEmpty() == false)
		{
			System.out.println("The generated clients are :  ");
			for (Client c : clients)
			{
				
				System.out.format("(%d, %d, %d); ", c.getID(),c.arrivalTime,c.getServiceTime());
			}
		}
	}
	

	public static void main(String args[]) throws IOException 
	{

		
		ArrayList<Client> clients = new ArrayList<Client>();
		
		Generator generator = new Generator();
		clients = generator.generateKClients("in-test-1.txt");
		

		Collections.sort(clients, new ArrivalTimeComparator());
		
		generator.printClientDetails(clients);
		
			
	
}
}

