package simulation;

import java.util.ArrayList;
import java.util.List;

import objects.Client;
import objects.Queue;
import workingStrategy.ConcreteStrategyTime;
import workingStrategy.Strategy;


public class Scheduler {
	
	
	private ArrayList<Queue> queues = new ArrayList<Queue>();
	private int maxNoQueues;
	private int maxClientsPerQueue;
	private ConcreteStrategyTime strategy = new ConcreteStrategyTime();
	
	
	public Scheduler(int maxNoQueues, int maxClientsPerQueue)
	{
		this.maxNoQueues = maxNoQueues;
		this.maxClientsPerQueue = maxClientsPerQueue;
		
		for(int i = 0; i < maxNoQueues; i ++)
		{
			Queue queue = new Queue();
			this.queues.add(queue);
			
		}
		
	}
	
	public ArrayList<Queue> getQueues()
	{
		return queues;
	}
	
	public void dispatchClient(Client c)
	{
		strategy.addClient(queues, c);
	}
	
	
    
	

}
