package simulation;

import java.util.Comparator;

import objects.Client;

public class ArrivalTimeComparator implements Comparator<Client> {
	
	public int compare(Client c1, Client c2)
	{
		return c1.getArrivalTime() - c2.getArrivalTime();
	}

}
