package simulation;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.BlockingQueue;

import objects.Client;
import objects.Queue;
import simulation.Generator;
import workingStrategy.Strategy;

public class SimulationManager implements Runnable
{
 
	 
	public int timeLimit ;
	public int maxProcessingTime ;
	public int minProcessingTime ;
	public int minArrivalTime;
	public int maxArrivalTime;
	public int numberOfQueues ;
	public int numberOfClients ;
	private Scheduler scheduler;
	private ArrayList<Client> generatedClients;
	volatile boolean start = true;
	volatile boolean flag = false;
	String outputFile;
	
	
	public SimulationManager()
	{
		
	}
	
	
	public SimulationManager(String inputFile, String outputFile) throws IOException
	{
		Generator generator = new Generator();
	
		ArrayList<Integer> integers = Generator.readFileInput(inputFile);
		
		this.timeLimit = integers.get(2);
		this.maxProcessingTime = integers.get(6);
		this.minProcessingTime = integers.get(5);
		this.minArrivalTime = integers.get(3);
		this.maxArrivalTime = integers.get(4);
		this.numberOfQueues = integers.get(1);
		this.numberOfClients = integers.get(0);
		this.scheduler = new Scheduler(numberOfQueues,numberOfClients);
		this.generatedClients = generator.generateKClients(inputFile);
		this.outputFile = outputFile;
		Collections.sort(generatedClients, new ArrivalTimeComparator());
	}
	
	
	public void listClients(ArrayList<Client> clientsList) {
		for(Client c: clientsList)
		{
			System.out.format("(%d, %d, %d); ", c.getID(),c.arrivalTime,c.getServiceTime());
		}
	}
	
	public void listClients(BlockingQueue<Client> clientsList) {
		for(Client c: clientsList)
		{
			System.out.format("(%d, %d, %d); ", c.getID(),c.arrivalTime,c.getServiceTime());
		}
	}
	
	public void printOutput(int time, String outputFile) throws IOException
    {
		PrintWriter pw = new PrintWriter(new FileWriter(outputFile, true));
		
									
    	try
		{   pw.println();
    		pw.format("\nTime %d ", time);
    		pw.println();
    		
    		System.out.println("\nTime " + time);
    		
    		pw.print("Waiting clients : ");
    		System.out.print("Waiting clients : ");
    		
    		if(generatedClients.isEmpty() == false)
    		{
    			for(Client c: generatedClients)
    			{
    				pw.format("(%d, %d, %d); ", c.getID(),c.arrivalTime,c.getServiceTime());
    			}
    			listClients(generatedClients);
    		}
    		else
    		{
    			pw.format("No clients are waiting");
    			System.out.println("No clients are waiting");
    		}
    		pw.println();
 
				int i = 1;
				int j = 1;
				for(Queue q : scheduler.getQueues())
				{
					System.out.println();
					pw.format("Queue %d :",i++ );
	 			System.out.print("Queue " + (j++) + ": ");
					if(q.getClients().isEmpty() == false)
					{
						for(Client c: q.getClients())
						{
							pw.format("(%d, %d, %d); ", c.getID(),c.arrivalTime,c.getServiceTime());
							
						}
						pw.println();
						
						 listClients(q.getClients());
					}
					else
					{
						pw.print("closed\n");
						System.out.print("closed");
					}
				}
				pw.println();
				//System.out.format("\nAverage Waiting Time : %.2f ",AVGWaitingTime());
		}
		catch(Exception e)
		{
			System.out.println("Writing in the file was unsuccesfull\n" + e);
			
		}
    	System.out.println();
    	pw.close();
    }
	
	public void refreshFile(String outputFile) throws IOException
	{
		PrintWriter pw = new PrintWriter(new FileWriter(outputFile,false));
		pw.write("");
		pw.close();
	}
	
	
	public double AVGWaitingTime()
	{
        double result = 0;
        double waitingTimes = 0;
        
        
        for(Queue queue : scheduler.getQueues())
        {
        	waitingTimes = waitingTimes + queue.GetAverageWaitingTime().doubleValue();
        	
        }
        result = waitingTimes / numberOfClients;
        return result;
        
    }
	
	public void run()
	{
		int currentTime = 0;
		try {
			refreshFile(outputFile);
		} catch (IOException e2) {
			System.out.println("Could not refresh the file");
		}
		while(currentTime < timeLimit && start == true)
		{
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				printOutput(currentTime, outputFile);
				
				@SuppressWarnings("unchecked")
				ArrayList<Client> copyGeneratedClients = (ArrayList<Client>) this.generatedClients.clone();
				for(Client c : copyGeneratedClients) 
				{
					if(currentTime == c.getArrivalTime() - 1) {
						scheduler.dispatchClient(c);
						generatedClients.remove(c);
					}	
				}
				int counter = 0;
				if(copyGeneratedClients.isEmpty() == true)
				{
					for(Queue q : scheduler.getQueues())
					{
					   if(q.isEmpty() == true)
					   {
						   counter++;
						   if(counter == scheduler.getQueues().size())
						   {
							   flag = true;
						   }
					   }
					   else
						   flag = false;
				    }
				}
				
				if(flag == true || currentTime == timeLimit -1)
				{
					PrintWriter pw = new PrintWriter(new FileWriter(outputFile, true));
					pw.format("Average Waiting Time : %.2f ",AVGWaitingTime());
					System.out.format("\n Average Waiting Time : %.2f ",AVGWaitingTime());
					start = false;
					pw.close();
				}	
			} catch (IOException e) {
				System.out.println("\n Nu a mers \n");
				e.printStackTrace();
			}
			currentTime++;
		}
		
	}
	

	

	public static void main(String args[]) throws IOException 
	{
		//SimulationManager sm  = new SimulationManager("in-test-1.txt","justTesting.txt");
	    SimulationManager sm  = new SimulationManager(args[0], args[1]);
	    Thread t = new Thread(sm);
		t.start();
		
	}
	
	
	
	
	
}