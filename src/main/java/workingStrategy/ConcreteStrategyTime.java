package workingStrategy;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

import objects.Client;
import objects.Queue;;

public  class ConcreteStrategyTime implements Strategy
{
	
	
	public void addClient(ArrayList<Queue> queues, Client client)
	{
		int minTime = 9999;
		Queue minTimeQueue = new Queue();
		for(Queue q : queues)
		{
			if (minTime > q.getWaitingPeriod().get())
			{
				minTime =q.getWaitingPeriod().get();
				minTimeQueue = q;

			}
		}

		minTimeQueue.addClient(client);
		
		
		
	}
   
}
