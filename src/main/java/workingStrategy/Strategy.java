package workingStrategy;

import java.util.ArrayList;
import java.util.List;


import objects.Client;
import objects.Queue;

public interface Strategy
{
	
	public void addClient(ArrayList <Queue> queues, Client client);

}
