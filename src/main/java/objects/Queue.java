package objects;


import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import objects.Client;





public class Queue implements Runnable
{
	private LinkedBlockingQueue<Client> clientsQueue ;
	private AtomicInteger waitingPeriod ;
	private AtomicInteger AVGWaitingTime ;
	volatile boolean flag = false;
	
	
	
	
	public Queue()
	{
		this.waitingPeriod = new AtomicInteger(0);
		this.AVGWaitingTime = new AtomicInteger(0);
		clientsQueue = new LinkedBlockingQueue<Client>();
	}
	
	public void addToFinishTime(Client newClient)
	{

        Iterator<Client> clientIterator= clientsQueue.iterator();

        while(clientIterator.hasNext() ){
            Client nextClient = clientIterator.next();
            newClient.getAndAddFinishTime(nextClient.getServiceTime());
        }

    }
	

	public void addClient(Client newClient)
	{
		clientsQueue.add(newClient);
		waitingPeriod.addAndGet(newClient.getServiceTime());
		addToFinishTime(newClient);
		if(flag == false)
		{
			flag = true;
			Thread thread = new Thread(this);
			thread.start();
			
		}
	}
	
	public LinkedBlockingQueue<Client> getClients()
	{
		return this.clientsQueue;
	}
	
	public void removeClient()
	{
		if(clientsQueue.isEmpty() == false)
		{
			clientsQueue.poll();
		}
	    
	}
	
	public boolean isEmpty()
	{
		if(clientsQueue.isEmpty())
			return true;
		else
			return false;
	}
	
	
	public void terminateThread()
	{
		flag = false;
	}
	
	public void computeAverageWaitingTime(Client client)
	{
		AVGWaitingTime.addAndGet(client.getFinishTime());
	}
	
	public AtomicInteger GetAverageWaitingTime()
	{
		return AVGWaitingTime;
	}
	
	public AtomicInteger getWaitingPeriod()
	{
		return waitingPeriod;
	}
	

	public void run()
	{
		while(flag == true)
		{
			try {
				Thread.sleep(1111);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!clientsQueue.isEmpty())
			{
					waitingPeriod.decrementAndGet();
					clientsQueue.peek().decrementServiceTime();
					if(clientsQueue.peek().getServiceTime() == 0)
					{   
						//computeAverageWaitingTime(clientsQueue.peek());
						computeAverageWaitingTime(clientsQueue.peek());
						clientsQueue.poll();
					}
			}
			else
			{		
				terminateThread();		
			}		
		}			
	}
	
	

	
}
