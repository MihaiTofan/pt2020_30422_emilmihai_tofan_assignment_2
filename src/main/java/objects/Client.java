package objects;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;



public class Client {
	
	public int ID;
	public int arrivalTime;
	public int serviceTime;
	public int finishTime;
	
	
	public Client()
	{
		
	}
	
	public Client(int ID, int arrivalTime, int serviceTime) 
	{
		
		this.ID = ID;
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
		this.finishTime = serviceTime;
	}
	
	public int getID()
	{
		return this.ID;
	
	}
	
	public int getArrivalTime()
	{
		
		return this.arrivalTime;
		
	}
	
	public int getServiceTime()
	{
		
		return this.serviceTime;
	}
	
	public int getFinishTime()
	{
		return this.finishTime;
	}
	
	public void getAndAddFinishTime(int value)
	{
		this.finishTime += value;
	}
	
	public void decrementServiceTime() 
	{
		this.serviceTime -- ;
	}

	
	
}

